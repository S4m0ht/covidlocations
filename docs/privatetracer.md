# Private Tracer

- [Specs documents](https://docs.google.com/document/d/16Ks_F23ivjfeTzZ6ZyRNTMpeNsA77lKbLiCTy_F1Ld8/edit#)
- [Odyssey Community](https://www.odyssey.org/pandemic-response-ecosystem/)


The process as described for now in the specs works like this:
- every device generates a key pair for each interaction, so device A will have A_i available and B will have B_j available
- each device after an interaction stores locally an encrypted payload, the payload is encrypted with the other device key, so A will have a payload signed with B_j and vice versa. The payload contains optional metadata on the interaction, like name, contact details, etc. The interaction is completed with timestamp and location in clear.
- when a device is contagious, by pressing the "I am contagious button" all the interactions are loaded onto C
- along with the interaction A will also give the private key A_i encrypted for B_j
- B can then decrypt its local payloads and read the meta information about the interaction with A
- all devices regularly downloads the list of contagious interactions from C

The main issues with this model are:
- when B receives the encrypted private key A_i, he can impersonate A by using A_i to sign his payloads
- there is no reason  for B  to read metadata about A, B just needs to know he was in touch with someone contagious. This metadata is optional, nonetheless it should be omitted, not even optional
- discovering one key (say B_i) you are implicitly also giving away the security  of another key, A_i in our example, because whoever has access to A_j has also acces to B_i. This is not necessarily a hole but still looks bad
- say that you want to workaround the impersonation problem, making that only the first time a key appears on C is the "valid time", every subsequent time it will be discarded because someone is trying to impersonate. This means that every time a party receives a public key (A and B during a handshake or C when receiving the interactions) the party must check against the public list of infected keys to see if that key was not already there. Since the model creates a keypair for each interaction, this thing is log(n) where n is the number of known interactions. It's not a disaster but it's definitely a pain point because n will be huge with viruses like Covid-19. Besides, there are scenarios where a malicious B can still cheat D by using A_i to sign and hoping D didn't syncronise it's contagious list yet
- there is no clear way to describe the social graph, only people giving access to their private keys allow C to build a graph by knowing which keys belong to a single person (same for the geolocation graph)


#### Patient history
A patient arriving at the interview with the healthcare personnel will have a list of it's interactions in terms of time and location, which are in clear on the device. If the other people who got in contact agreed, the device will also see in clear the contact details signed by other users during the interactions.

On one side it may be interesting to have in-clear data of people who got in contact with the patient, on the other side this is extremely suspicious and possibly unnecessary, the people who got in contact with the patient should have been notified by the system already, collecting their contact details seems unnecessary (the virus indeed doesn't care about your email address)

The location in clear may also have upsides since it will be easier to recognise certain places (if an address is resolved from coordinates), on the other hand for covid-19 the specific location of the contagion is not relevant. It could be relevant for viruses which cannot live above certain temperatures or can transmit only with arid climate and so on. 

Specific information about where the patient has been can be asked to the patient directly, I'll leave an open point to discuss with doctors how is it relevant to know exactly where possible contagions have happened. Notice that the beginning of the spread is the most interesting time to track with precision, but the reason is usually to understand who else could have been in contact there.

#### Alerts for users

Users can publish their status to the  network without waiting for the medical inspection, this is speeding up the time needed for a user to know whether it was in contact with an infectious person.

The location are signed so users know that they were indeed in contact with this other person, it's impossible to generate fake interactions without the user actually being in proximity of the other user.

The downside of the user-generated reporting is that users are not as precise as medical personnel in reporting their health so they can do honest mistakes which tints the data with errors.


#### Epidemiology
For C it's not possible to know how many people are currently tracked because C doesn't know how many keys belong to a single person. So a patient uploads his/her infectiour interactions and C has to wait for other patients to also report in to understand if those interactions happened with 1 - 10 - 100 people.

The topology both of the social graph and of the location graph can be constructed only after people turn into hospitals and health personnel collect data from the devices. 


#### Untruthful user reporting
- So Bob is sick but he won't push the button. If everyone is like Bob there is not much to do but wait that C hospitalizes one of these Bobs and the healthcare official will use his/her device to handshake with Bob and then push the button. In the case of Covid-19, part of the population is indeed hospitalised so this scenario is not impossible. 
- Bob finds out that he was in touch with a contagious key, this means that also his key is now contagious. He may want to remove from his device all the interaction with that key, now, when he will upload his interactions, he will have only "clean" interactions. There is no way for C to know that Bon removed those interactions, C will notice that some interactions of A don't have a match with other users yet.
- Bob is not sick, but he really wants to be for whatever reason, so he pushes the button. The only mitigation in this scenario would be for someone to test Bob, assess he is healthy and undo the screening process of the locations done by C. 

#### Troll farms
- Malicious users can create as many public private keys as they want, nonetheless they need at least one person phisically in a place to start reporting data about that place (because they cannot sign with keys that are not their own). Even if they do send a single guy to the physical location, C will see in a place a user which is the single point of contact between two populations, the real one and the troll one. So having a single key signing a lot of locations for other devices is an indication of a strange behaviour (this is similar to the hashed name situation because the interaction requires a signature from the other party).
- interestingly though, since there is no seed negotiation, the troll farm can generate syntethic data on the exact same locations of the real users, so C must be aware to observe this separate populations of keys. It would be quite suspicious that on the same coordinates there are some keys that never interact (unless there's a guy on site, see point above). Finally though, with this model location is in clear only after a user gives access to its local storage to the healthcare personnel.
- The considerations on the fingerprints are basically the same, C can see if there are recurrent fingerprints uploading lots of interactions

#### Private data leakage
- C actually sees very little of the users, because C only receives from device A a list of public keys it interacted with and encrypted private keys. 
- On the other hand B and all other devices which can decrypt the payloads will read additional information about A, it's not clear what is the purpose of this data for B.
- The private key impersonation is on the other hand a pretty bad design, the impersonation may not happen a lot in practice but in concept it's annoying and working around it will indeed slow down the system