# Anonimity Sets

The basic idea is that in an anonimity set each data point looks exactly as another so the central server C can bind the fingerprint of a device only to a set of possible identities, not exactly to one. It is actually easy to build workarounds where you can engineer anonymity sets and after some iterations understand who is who, our design should try to mitigate this attack. Another important aspect for an anonimity is to make it difficult to generate by a single entity (e.g. troll farm with 10 mobiles available). First we describe the process, then we go through a discussion about how the method fares in the desired use cases and in the attack scenarios.

*Collecting the interactions and close quarter policy:*
- Every user generates its own priv/pub key pair, we use 1 key per user to avoid data to explode and for initial simplicity
- Each device  has a local storage of "keys you have met" in the last X days (KYHM set)
- An interaction between two devices creates two entries in each KHYM set
- Each entry is composed of
  - the user public key
  - a timestamp negotiated by the two devices
  - the current courtesy state of the device (*infectious/non infectious)
  - a signed message which is the concatenation of the three above, signed by its respective private key
- Notice that both devices will carry two of such entries, one for each user so that both A and B have in their KYHM set A and B entries.  The entries look exactly the same in the two devices, this is by design, the  summetry is required to generate  meaningful anonymty sets.
- (e.g. obvious checks: before adding the key in the set the device should check if that key was already met within the past hour or anyway before uploading to C)

*Sending interactions to C:*
- the device signs with a _1-of-n_ ring signature the hash of the current anonimity set. _n_ is the set of public keys in the anonimity set, the private key used is the owned key (this  step is useful only against cheating Bob).
- the device uploads it's interactions, some solutions describe this process as happening when the user is positive to the virus, there are pros and cons, for now we propose this happens when there are at least Y records in the KYHM set, so that the user is anonymous "enough"
- C looking at the incoming sets can indeed bind sets to device fingerprints

*Pushing the "I am contagious" button:*
- the device notifies C that its owner is contagious
- C marks the fingerprint as "contagious"
- C scans through all the anonimity sets that were submitted by that fingerprint and collects all the public keys that were in those anonimity sets (may be limited by time?)
- C publishes that list of public keys as the contagious list
- Clients will trust C to be truthful about the contagious list (this is our assumption)

#### UX
> What is the courtesy state? Of course A cannot trust B just saying he's infected, nonetheless if B is willing to share it there is value in warning nearby people that they have to be careful for contagion risk. 
> Is this more dangerous than useful? Probably, nonetheless we live in the now, so it's important to have this information now. When I already have the  virus, I already have it and there is nothing else I can do, but when I don't have it I may decide to be extra careful when someone sick is nearby. So the courtesy state is a self assessment of each user if they want to share it for real time interactions. Courtesy states are of course completely unreliable even in good faith (maybe you just have a normal flu).
> From the UX  point of view the app may make the device vibrate or something to notify someone contagious is reported nearby.

> When too many keys are added within the same hour, the device can also send a warning to the user, there is indeed something strange about meeting all these people

#### Patient history
When the healthcare personnel inspects the patient they can ask the patient if on the app he was notified as being someone in touch with contagious people, the local app can also know when a contagious key was in one of its anonymity set, therefore giving a timeframe for the infection. The healthcare personnel may also access a special portal offered by C which shows when a fingerprint became contagious and therefore add details to the patient history.

If the patient was not (yet) contagious for the app, they can ask the patient to push the button and notify all the network of his/her state. The healthcare personnel can ask all the questions they consider relevant directly to the patient, the patient eventually may save his/her history on the device so that it can be inspected but there is no need to pass this sensible data to C or to other users.

#### Alerts for users

Users can report their status and warn the network about it. It is impossible for users which were not in contact with another user to generate synthetic data faking the interaction. So users can be notified by the system as soon as another users reports its status.

Courtesy status for real-time proimity warning has important impacts on social distancing behavior but consequences that should be clearly explained to users (stigma, aggression, etc..)
The issue, that is intrinsic for user-assessed health status, is that users are not as accurate as medical personnel.


#### Epidemiology
The community health state is summarised in C, which has a list of contagious keys, the total list of known keys and an approximate contagion history starting from the first fingerprint which reported as being sick. From the topology point of view it is possible to build a graph between public keys to see how many people on average have been in contact with a key. It will be also possible to spot the keys which have the most contacts and eventually drill down on which fingerprint reported such keys the most times across different anonimity sets. 


#### Untruthful user reporting
- So Bob is sick but he won't push the button. If everyone is like Bob there is not much to do but wait that C hospitalizes one of these Bobs and the healthcare official will use his/her device to handshake with Bob and then push the button. In the case of Covid-19, part of the population is indeed hospitalised so this scenario is not impossible. 
- Bob finds out that he was in touch with a contagious key, this means that also his key is now contagious. He may want to remove that key and his key from all of his anonimity sets. If the sets were already published to C, it's too late, C already warned the other users. But if Bob has still to upload anything to C he can try to remove those key, but now he can sign the 1-of-n only with his own private key which now is not represented in the ring signature, so he just can't participate in the network if he wants to cheat.
- Bob is not sick, but he really wants to be for whatever reason, so he pushes the button. The only mitigation in this scenario would be for someone to test Bob, assess he is healthy and undo the screening process of the anonimity sets done by C. 

#### Troll farms
- Malicious users can create as many public private key as they want, producing fresh and credible fingerprints though is a more expensive game (of course everything is possible, e.g. cellar full of modems and routers, seen it).
- It would be easy to spot the same 10 devices with the same network connection producing bulks and bulks of new anonimity sets.
- The process of creating anonimity sets is not resilient to spam, it is very important to be prepared at the fingerprint level to identify tresholds of possible and plausible data sources against malicious data sources.

#### Private data leakage
- The fastest way to unveil immediately the link between: a user, its public key and its fingerprint is by inspecting the device and looking at the first anonymity set, then with the fingerprint check all following anonimity sets and try to see which key of the initial set is always appearing in the following sets. 
- No location is ever recorded so the location history of the user is preserved and can be asked directly to the user by the medical personnel (who will have anyway an indication on the time of the contagion, see _Patient History_)