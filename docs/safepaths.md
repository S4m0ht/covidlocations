
# Safe Paths / Private Kit

- [Safe Paths](https://safepaths.mit.edu)
- [Private Kit](http://privatekit.mit.edu)
- [App repository](https://github.com/tripleblindmarket/covid-safe-paths)

The process description reflectes that the code is an MVP in the early days and I couldn't find a specs document but just read through the repository. Therefore the observations may change in time depending on how it evolves, anyways:

- every device stores its location and relative timestamp, a polling for location can be activated and remain in background
- when a device publishes it's location history (for instance to confront it with another location history or to upload it to C)
  - for every point it approximates the coordinates and the timestamps so that they are quantised, let's say in ranges of 100m and 2h slots
  - for every point the device generates a random point in terms of coordinates and timestamp
  - for every point it computes the difference between the random point and the "quantized" real point dimension per dimension (so the delta in terms of lat, lon and timestamp)
  - the random point value is hashed (let's say the 3 values concatenated and then the string hashed)
  - the pair of "points difference" and "hashed random point" are exported for each point in its local history
- when a device receives a list of points differences and hashed random points, it can check if it was near those points as follows:
  - for every point in the local history approximate with the same thresholds (so the devices must know the 200m and 2h parameters) 
  - add the "points difference" to find a random point
  - hash this new random point and check if the hash is the same as the one passed by the other device
  - if yes, then the two devices have been close in that point
  - if for every point in the local history and every point in the received ones there is no match, then the two devices never were in the same place

In this scenario C can be an FTP point or some form of central server which collects the "hashed histories" of the devices.

The main issues with this model are:
- All the data can be synthetic, in fact there is no need for "witnesses" to generate such paths
- It would be trivial to create a fake trail: you decide when and where your points should be, prepare the hashed history and upload it as "infected" (important note: it is not really clear from the code if the "I am contagious button" is/will be available to tint a path as a contagious path)
- The paths are collected even without an interaction, this has upsides and downsides, downsides because in the scope of "covid-19 social distancing" this approach looks an overkill, upsides because indeed you can have a broader patient history that can be used to detect contamination places for poisons or other things.
- when many users adopt this app, it is quite clear that observers can detect the status of a device with a timing attack: if the device takes very long to find out if it was infected or not, then it can be non-infected, but if the operation ends "soon" it means a collision was found between the paths. This happens because the population will upload a lot of points so it will take a lot of time to check all of them (since they are rightfully hashed, you cannot really filter or prioriteze some points over others to focus the search and speed it up)

#### Patient History

The app has all the location trails and if the healthcare personnel has access to C (or anyway a list of contagious points), such personnel can indeed check if the device has been in contact with a contagious one. Notice that the personnel can see the location history anyway in clear so this can be an advantage to reconstruct specific journeys done by the patient. The social graph seems more relevant than the exact locations graph for "covid-19 social distancing" and for warning other possibly infected people, so exact locations may be an overkill for covid-19 but an interesting features for other pathologies. 

#### Alerts for users

It is not clear form the current implementation if the user can signal he/she is infected on its own and be warned if there was a possibile location for contagion in the history. If the patient signals the infection or the possible contagion only after the medical personnel inspection, the reporting has the advantage of being more accurate in terms of accurate data but lagging in terms of how long it takes for users to understand if they were actually in contact with a contagious user.

On the other hand, say the users can report infection on their own, both C and the users cannot distinguish between real locations and synthetic ones, so fake interactions are very easy to produce and the accuracy would be greatly reduced in this case.

#### Epidemiology

C can't know who was in contact with who: when a patient tints his/her points with "I'm infected", C cannot know who else was in contact and needs to wait for the users to report in. Also, C has to trust whatever the single user say, there is not way to validate that the reported path is authentic or synthetic.

The social graph and the topology can be built incrementally while people report in. The topology is very easy to interpret as it's exactly the geographical map.

#### Untruthful user reporting

- So Bob is sick but he won't push the button. If everyone is like Bob there is not much to do but wait that C hospitalizes one of these Bobs and the healthcare official can start to tint hashed locations with "infected" and then this can cascade onto users. In the case of Covid-19, part of the population is indeed hospitalised so this scenario is not impossible. 
- Bob finds out that he was in touch with a contagious location, because he downloads the infected paths and finds a match on his device, there is no way for C to know that Bob was in contact with a contagious location and Bob can remove the tinted locations from history and upload the "clean" path, there is no way for C to detect this behavior from Bob, Bob can still participate in the network.
- Bob is not sick, but he really wants to be for whatever reason, so he pushes the button. The only mitigation in this scenario would be for someone to test Bob, assess he is healthy and undo the screening process of the locations done by C. 

#### Troll farms
- Malicious users can create as many paths are they want, where they want, when they want.
- It is possible to create a path passing through critical infrastructure and signal it as contagious
- The considerations on the fingerprints are basically the same, C can see if there are recurrent fingerprints uploading lots of hashed paths, C wil have to extra careful because device will behave in different ways when there are a lot of points, revealing their status by the fact that the process to check to a lot or a little time (the sooner it ends, the more likely it's infected)

#### Private data leakage
- C a priori sees very little of the users, because C only receives from devices a list of hashed points
- On the other hand after a patient reports in C knows exactly the locations of its path. This means that C can understand who else (in  terms of fingerprints) uploaded points which touched points of A, this is the process with which C can start to build the social graph and also the contagion map
- After C knows about A, C knows exactly the locations