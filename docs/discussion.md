# Discussion

Different types of status have different meanings and functions:

- Self-reported vs Healthcare system confirmed
  - self reported status is faster to signal and therefore when the population is truthful is a valid tool to be quickly notified of possible contagions
  - healthcare system assessed status is more precise and more difficult to cheat: even if fake data can be injected from the apps, if those fake patients never show up they'll never be reported as sick and will never tint other data
- Real-time courtesy state
  - this state is a device signaling immediately to the neighbours that it's contagious
  - this option should be available to the user because it's probably the most useful one in terms of making effective social distancing
  - the users should not be able to detect exactly who is contagious (unless there are only 2 people around) 
  - the courtesy state can be completely "offline" and never be reported to C, nonetheless users shoudl trust each other saying the truth or anyway being updated about their own status


Mitigation for self-assessed truthfulness and availability:

- Using witnesses (the payload of one device is signed by another device) is effective but not sufficient to avoid the data being manipulated by troll farms. It is important that the signed data also carries a specific seed of the local interactions so that malicious users must be in the same place as the population to report data (when they do, if only one person is involved, the system should be able to detect that there is indeed a strange user making the bridge between two populations)
- Users that publish data only when they want to, on one hand is a good data sharing approach, on the other hand from an high level population overview it may not be as effective as gathering data costanttly. Regardless of the solution, the data extracted should clearly explain what it's representing, and, if it's representing only the fraction which actively share their data then statistical sophistication should be used (on 60M population, if only a couple of thounsands report in, maybe only from a major city, it's a pretty approximative data population-wise)


Single interaction keys vs user keys:

W.I.P.