
# PEPP-PT style

- [Pan European Privacy-Preserving Proximity Tracing](https://github.com/pepp-pt/pepp-pt-documentation/blob/master/10-data-protection/PEPP-PT-data-protection-information-security-architecture-Germany.pdf)
- [Android sample implementation](https://github.com/pepp-pt/pepp-pt-ntk-sample-android)

The process description reflectes what I could infer from the specifics and then checked on the available source code:

- when a user registers to the service a list of challenges is presented to prevent fake accounts
- once the user is registered, C creates a 16 bytes random number (PUID)
- regularly C creates random daily keys (BKi) which are valid for the i-th time frame (e.g. 1h)
- for every user C computes Ephemeral Bluetooth Ids (EBID) with  the user PUID and a set of time frames BKis
  - EBID = AES(BKi, PUID), this is done for many i, e.g. for 2 days worth of slots
- each user then downloads his/her EBIDs and their correspective time slots
- each user's device then starts broadcasting its EBIDs (in the correct time frame) and registering the EBIDs that are encountered
- when two devices interacts they register:
  - the respective EBID
  - the timestamp
  - metadata of the bluetooth connection like signal strenght and optionally WiFi state and screen state
- users cannot self report their health status, the medical personnel assess their status and then a code is given to the patient which will use it to upload the contact data details collected in the last 3 weeks.
- how the code to upload data (called TAN in the documentation) is given to the patient is not described because it depends on the healthcare system
- once C has the list of encountered EBIDs of the infected users, C procedes to find which users were in touch with him/her and sends them a notification. This process basically checks that the reported EBIDs were indeed from the interesting time frame and from them gets the users PUID
- in particular users can request 

Before looking  through the app I was very impressed by the documentation, among the projects I've reviewed this is the most comprehensive with details of how exactly the app should behave not only during the contact tracing but also during key moments like user registration and user notification.

The main issues with this model are:
- The users trust blindly what they receive via internet for their EBIDs, this opens up a discussion thread of which connection can be considered secure to download your EBIDs and how devices must absolutely double check the public keys of C when they open  the secure channel to download their EBIDs 
- There are two macro ways to deliver to users the notification of their risk score (how likely have you been in contact with someone contagious):
  - send a push notification
    - if you send a notificaiton, you better send it to everyone (not only to the infected), otherwise it's easy to identify new infected by listening to who is receiving notifications
  - wait for the app to regularly query for its risk score
    - the statuses should be computed for all users and only then sent out, if this is done with a  request/response, measuring the time can give away information, if the response is immediate either there is no change or there is no news, but the longer it  takes the longer it's likely C is finding one or more hits
- AES, although considered secure, it's the encryption system most widely studied, truth be told the highest performing attacks (to my knowledge) require to see what the processor is doing. C doesn't show it's computation time or cache, so even if a malicious user can indeed upload crafted packages of data, there would be no chance for the user to see what sort of computation C is doing with the crafted data. Even if the user could measure the time it takes for notifications to arrive, it would be trivial for C to just batch the notification at midnight for instance, without mentioning the notification service time which can also mess up the time measurements.
- C knows everything and can impersonate any user if C wants to. Probably C doesn't want to, but still this is itchy.
- I understand that how TAN codes (the ones to upload the contact history) are given to patients may vary, but still you have to be sure that the correct TAN is given to the correct user, possibly without other third parties knowing it.
- This approach requires internet connection regularly for the device, because they can't generate their ids


#### Patient History

The app contains a risk status of the patient which can be used by the medical personnel to infer the degree of exposure of the patient, C can also prepare a view for a given user of it's most dangerous contacts in a timeline, which can help healthcare professional assess the status of the patient. It's not possible to reconstruct the phisical journey of the patient without intervewing him/her.

#### Alerts for users

Users cannot report symptons or infection status on their own: this removes false positives from the network but the users receive feedbacks on their past only after the testing is done on their contacts. There is no close to real-time interactions predicted for now.

#### Epidemiology

C known everything about the users so can also know who was in contact with who after the first infected user reports in. C can build the social graph and see immediately who was in contact with the infected user.

Unless meta-data is provided to C, it's impossible to build a topological map of the infection.

#### Untruthful user reporting

- So Bob is sick but he won't push the button to signal the netowrk. There is no button to push, Bob will eventually be hositalised and the medical personnel will give Bob a code, Bob can still decide not to upload its data. Nonetheless C knows everything about Bob and can create the contagious keys and warn the rest of the network.
- Bob finds out that he was in touch with a contagious key, because he receives a notification or sees his risk score, there is no way for Bob to hide this from C, because C known which EBIDs are Bob's ones even without Bob reporting in.
- Bob is not sick, but he really wants to be for whatever reason. Bob has to guess a contagious EBID, truth be told, Bob just needs to go to the market and stay there an hour, but a part from this there is no other way

#### Troll farms
- The users registration challenges help indeed to reduce the presence of bots, although troll farms are also human based 
- Fake contagious contacts are impossible to inject with every model using medical personnel to "approve" the contagious status
- On top of that, it's also impossible for troll farms to generate arbitrary numbers of keys and/or contacts, because keys are generated by C

#### Private data leakage
- C knowns can impersonate users when it's about keys but has no access to other information about the user, eventually C will receive the TAN codes and link those to the user so whatever information is given with the TAN will also reach C (maybe none, maybe hints on the clinical history, it's not clear who and how generates the TAN codes)
- Users can't really know much about which user got them the contagion, in fact devices download sort of "anonimity sets" where all daily keys looks the same.
- Depending on the implementation, listening the network for notifications and looking at who is receiving notificaitons can be a tell sign of the current user's health. Also looking at the average time between the request for risk and the answer from C, depending on the implementation, can give away some information.