# Privacy-preserving approaches

The two main family of approaches identified so far:
- [anonymity sets](anonimitysets.md), a device never uploads information only about the owner but always uploads a batch of information where noone (possibly not even himself) can detect what data belongs to whom.
- [hash and salt locations](hashandsalt.md), inspired by the birthday attack, you only need to know two people were in the same place, you don't need to know the exact place. This is basically an extension of various existing protocol for secure location sharing.

## User preferences

Summary of the papers describing the tradeoffs between user burden on setting privacy and user desires of sharing their location (see references in [State of Art](stateofart.md))

## Ethic Choices

The point would be to make it clear why preserving privacy is important and why users can't really be  tasked with such a great effort, the system should protect it "by design". Definitely not my field, but there is some literature also on this topic to be collected.

W.I.P.