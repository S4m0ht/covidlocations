## State of Art

A review of some approaches under development and a look into papers dealing with "private location" sharing.


### Covid-related projects

Existing projects:

- [Private Tracer](privatetracer.md), each device creates a new key pair for signing its part of an interaction with each other device
- [Safe Paths](safepaths.md), each device stores locally its locations and shares an hashed version that can be compared only by devices having the same locations
- [Safe Trace](safetrace.md), based on Google location service, the location data is encrypted before exporting to secure comptaution (W.I.P., not analised yet)
- [Contact Tracing Apple & Google](contacttracingAG.md), each device creates its own identifiers to share during interaction, inspired to the [DP-3T](https://github.com/DP-3T/documents) style. Location is not esplicitly collected (although see Moxie's first look [https://twitter.com/moxie/status/1248707315626201088?s=20](https://twitter.com/moxie/status/1248707315626201088?s=20))
- [PEPP-PT](pepppt.md), a central server provides IDs for the devices, the techinique is used to safeguard the system against bots and fake data

Our proposals:

- [Anonimity Sets](anonimitysets.md), a device never uploads only its data, devices always upload data from multiple entities
- [Hashed and salted maps](hashandsalt.md), devices in proximity negotiate a salt before hashing the coordinates

### Privacy and locations outside Covid-19 scope

A brief review of some papers from academia or the industry about this topic, please suggest other interesting work

- [Albatross](https://arxiv.org/abs/1502.03407), 2015 position paper by Samsung and Intel where some basic protocol (Albatross) functions are described to exchange encrypted locations between contacts and to introduce dummy traffic to make it difficult to analyse recurrent traffic from a user
- [Capturing Location-Privacy Preferences: Quantifying Accuracy and User-Burden Tradeoffs](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.841.8060&rep=rep1&type=pdf), 2010 Carnegie Mellon users survey that describes preferences on when people is comfortable sharing location data and how much effort users are willing to put in to define their privacy boundaries
- [Mitigating Trade-offs Between Accuracy and User Burden in Capturing Mobile App Privacy Preferences](https://content.sciendo.com/view/journals/popets/2020/1/article-p195.xml), 2020 followup study of the initial tradeoff analysis
- [Secure Multi-party Computational Geometry](http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.11.3591) 2001 seminal paper with a specific protocol to have a secure communication about querying if two poligons intersect without revealing where 
- [Centralized Privacy-Preserving Computation of Users’ Proximity](http://homes.di.unimi.it/~mascetti/papers/09-SDM.pdf) 2009, the paper describes a protocol to help a third (centralised) party inform users if they are nearby without revealing the exact locations to this third party

### Comparisons

For each covid project we want to summarise how it fares along our 6 dimensions to extract pros and cons

W.I.P.
