# Virus tracking and preserving privacy

A number of projects is proposing to track device locations/interactions and disclose them only when relevant for medical purposes. Although with different formulations the basic concept is: as long as you are healthy your data is private. Is it possible to achieve the same or better results of that policy without violating user privacy at all?

The aim of this document is to highlight:
- the use case for data collection
- possible attack scenarios
- comment on the existing implementations or specifics
  - see [State of Art](docs/stateofart.md)
    - [Private Tracer](docs/privatetracer.md)
    - [Safe Paths](docs/safepaths.md)
    - [Safe Trace](https://blog.enigma.co/safetrace-privacy-preserving-contact-tracing-for-covid-19-c5ae8e1afa93)
    - [Contact Tracing Apple & Google (DP-3T style)](docs/contacttracingAG.md)
    - [PEPP-PT](pepppt.md)
- propose other two privacy-preserving approaches
  - see other [Privacy-Preserving Approaches](docs/preservingprivacy.md)
    - [Anonimity Sets](docs/anonimitysets.md)
    - [Hash and salt locations](docs/hashandsalt.md)

Each approach will be valued on six dimensions:
1. *Patient History*, how useful is the tool in helping medical personnel
2. *Alerts for users*, how fast and  accurate can be the system to deliver a warning to a user if the user was in contact with someone contagious
3. *Epidemiology*, how reliable and how quickly it can give an overview of the population
4. *Untruthful user reporting*, what if users are not incentivised to tell the truth about their health? Is the model resiliant to this?
5. *Troll farms*, users may try to inject bogus data to skew the landscape, how difficult is it to create and inject synthetic data?
6. *Privacy*, who else other than the patient can see private data? When is data in clear?


## Use Cases and Assumptions

Objectives to be accomplished:
- warn users (as close to real time as possible?) of being in a contagion-likely situation
- warn users they have been in contact with someone contagious at most X days ago
- provide epidemiologists with some sort of topology map of the spread and KPIs to understand if policies are effecgive (is social distancing working? Etc..)
- provide healthcare professionals with an outlook on how many times the patient has been in contact with contagious people

Assumptions:
- relevant data for a diagnosis is the number of contagious interactions and possibly how many contagious people were around (other data may be relevant like temperature, humidity..) 
- the exact contact details of people and coordinates of the interaction are not relevant for the virus, therefore should remain secret or not tracked
- data on the mobile phone is accessible to the owner which can add, remove, edit, whatever he wants inside the application to trick the system if he/she wants to
- for semplicity we can rely on a third central party (C) which orchestrates the data flows, for instance in the Private Tracer proposal it may represent a server where people upload their interactions and download contagious interactions, anyway we assume that we can design with a central server executing some logics and sending alerts to clients when necessary, as long as we treat our private data as forever private and only give this central server the minimum information to execute for the described use cases. This central server has external incentives in making  a good job at truthfully warning people when they are at risk of contagion, nonetheless this central server may or may not disclose data to other parties in the future, so we want to design keeping it in mind. 
- when using a mobile device for communicating with a server over the internet the user implicitly discloses to its operator (and eventually to C) his/her network fingerprint, phone number, approximate location (by phone network) and contract details. 
- there is at least a part of the population which is willing to report truthfully when they get sick

Last but not least:
- if some data is non-neccessary for the system, either because it's completely non relevant or because it will be collected anyway by the instructed authority (e.g. health professional performing duty), the system doesn't collect it and makes it as difficult as possible for anyone to reconstruct/guess it.


## Possible attack scenarios

We live in an era where information is a weapon, like/believe it or not, so it's only right to go through a couple of scenarios and see how the solutions fare:

- State-sponsored dirty data: troll farms may as well downlaod the app and use it to report bogus data, evenutally even generating bogus locations
- The health system, or whoever is running C, may leak location data for other purposes other than the fight to the virus
- Misrepresenting personal health: an individual may or may not want to share the fact of being contagious truthfully, he/she may conceal it to avoid social stigma or simulate it to receive a health bonus, etc..
- Eavesdropping: someone may listen to the network to infer the status of a device (was it in contact with a contagious device? Was it not?), listening may mean trying to extrapolate information from packages size, response time, etc..

## General definitions

We follow this nomenclature:

- a close quarter interaction between two devices is _interaction_
- A (Alice), B (Bob) are users with a device, B sometimes cheats (sorry Bob, someone gotta be that someone). C (Central server) collects data from all users which send to it and elaborates at will. D (Demetrius) is like Alice, but needs to be someone else.
- signed message _m_ with private key _i_ is $\sigma_i(m)$, more complex signatures if present will be described (e.g. 1-of-n, etc..)
- the hash of a message _m_ is hash(m), let's say SHA256
- device fingerprint is a mix of network and hardware traces that the device leaves when it interacts with something over the internet

## Discussion

Finally some highlights of the findings are reported in [Discussion](discussion.md)

- self reported vs healthcare system assessed statuses
- recommendations on fingerprints
- ... W.I.P.






