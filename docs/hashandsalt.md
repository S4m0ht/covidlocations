# Hash and salt locations

Let's say you just want to know if a number of people out of a population where in the same place, without needing to know the exact place. So the key aspect of this proposal is to make devices agree on an encoded name for the time and place where their interaction  happened. This will allow to create more detailed topographies compared to the anonimuty set where you just see who met who but cannot distinguish if three people met in the same place of two by two in different places. The fact that a topography can actually be built will make it easier to observe if social distanting norm are taking effect. With the anonimity sets you can observe a reduction of the sets size, but it will be difficult to do it timely because people may not upload their interactions until they reach a desired anonymity set size. So a big difference from anonimity sets is that real time information can be observed about users (of course it has to be built in a way that it's not discolsing sensitive data).

*Collecting the interactions and close quarter policy:*
- Every user generates its own priv/pub key pair, we use 1 key per user to avoid data to explode and for initial simplicity
- Each device  has a local storage of "places you have been" in the last X days (PYHB set) which can be even syncronised real time with C
- An interaction can be prefaced with sharing the courtesy state of the device (same concept as for the anonimity set)
- An interaction between two devices begins a negotation to decide how to name the place in terms of:
  - approximated geolocation  (say for simplicity, to the 3rd decimal degree: 0.001 ~100m)
  - timestamp
  - random salt (I've improperly used _seed_ here and there while referring to the _salt_)
- Each device loads the interactions it had within 100 meters of the current location in the last 1 hr and ranks each of their "approximated geolocations" from the most used to the least used
- Two lists are merged, each device removes the approximate geolocations which are further than 100m
- Is there an approximate geolocation remaining? If yes, concatenate it, its timestamp and seed to create the message m to hash. hash(m) is the hashed name of the location
	- If not:
	- average the locations of the  two devices and  truncate the result, this is the new location
	- the highest timestamp between the two devices is the new timestamp (given that it's within one hour of the other, say it's not, the handshake ends and no location is registered*)
	- to generate the seed use the  truncated values of the geolocation lat and lon, the timestamp and the two public keys signed by their respective owerns to generat _m'_, the last X characters of _hash(m')_ is the seed (this seed has to be difficult to guess if the device is not physically in the same place gossping with other devices in the area)
	- concatenate approximate location, timestamp and seed to generate message m, hash(m) is the name of the location
- Each device signs the location with its own private key and gives the signed copy to the other device (basically acting like a witness)
- the interaction is saved as hashed name, approximated location, timestamp, seed. Approximate location and seed will never be disclosed to C.

*Sending interactions to C:*
- the interactions are sent with a delay from devices to C (*: see important notes in the privacy chapter)
- an interaction sent to C is composed of the hashed name of the location, timestamp, public key of the device, public key of the signer device, signed hashed name of the location
- C expects to receive these interactions in pairs
- C can indeed bing fingerprint information to these interactions and also can have an advantage in guessing the location because C can know the mobile cell used to transfer the data from the device to the internet

*Pushing the "I am contagious" button:*
- the device notifies C that its owner is contagious
- C marks the fingerprint and the public key as "contagious"
- C adds the public key to the list of public contagious keys
- Clients will trust C to be truthful about the contagious list (this is our assumption)

*Avoiding neigbour scans and recurring locations:*
- of course a patient has been at his/her home, it is not very interesting to report location in the surrounding of "home"

#### UX
> when a user accesses for the first time, the user should be asked to provide an area where interactions are not registered, "private/home area"

> When too many interactions are added within the same hour, the device can also send a warning to the user, there is indeed something strange about meeting all these people

> Courtesy state can also be introduced, as explained in [Anonimity Sets](anonimitysets.md)

#### Patient history
When the healthcare personnel inspects the patient it is possible to see if the public key has been marked as contagious and also a timeline of (hashed) places where the patient has been. The healthcare personnel may also access a special portal offered by C which adds details to the patient history.

If the patient was not (yet) contagious for the app, they can ask the patient to push the button and notify all the network of his/her state. The healthcare personnel can ask all the questions they consider relevant directly to the patient, the patient eventually may save his/her history on the device so that it can be inspected but there is no need to pass this sensible data to C or to other users.

#### Alerts for users

Users can report their status and warn the network about it. It is impossible for users which were not in contact with the user to generate synthetic data faking the interaction. Users can be notified by the system as soon as another users reports its status. The issue, that is intrinsic for user-assessed health status, is that users are not as accurate as medical personnel.

Courtesy status for real-time proimity warning has important impacts on social distancing behavior but consequences that should be clearly explained to users (stigma, aggression, etc..)


#### Epidemiology
The community health state is summarised in C, which has a list of contagious keys and a list of places where the patients have been. 
- The list of places allows to build a topography of the contagion which is not only the social graph, the epidemiologist will be able to distinguish where two keys got in contact. Eventually they can ask healthcare personnel to investigate certain keys and understand where exactly they where at a point in time. 
- Moreover these hashed and salted locations can be uploaded almost in real-time and don't need to wait for the user to upload them, which creates a better imaging on the population.


#### Untruthful user reporting
- So Bob is sick but he won't push the button. If everyone is like Bob there is not much to do but wait that C hospitalizes one of these Bobs and the healthcare official will use his/her device to handshake with Bob and then push the button. In the case of Covid-19, part of the population is indeed hospitalised so this scenario is not impossible. 
- Bob finds out that he was in touch with a contagious key, this means that also his key is now contagious. He may want to remove from his device the location where the contagious key was met, he can do it since he knows the public key, but he cannot remove the same location he signed when handshaking with the other device because it's indeed on the other device. C eventually will see that Bob is not uploading his locations.
- Bob is not sick, but he really wants to be for whatever reason, so he pushes the button. The only mitigation in this scenario would be for someone to test Bob, assess he is healthy and undo the screening process of the locations done by C. 

#### Troll farms
- Malicious users can create as many public private keys as they want, nonetheless they need at least one person phisically in a place to start reporting data about that place (because they cannot negotiate the seed otherwise, they can make up all the rest, but it should be difficult for them to guess the seed). Even if they do send a single guy to the physical location, C will see in a place a user which is the single point of contact between two populations, the real one and the troll one. So having a single key signing a lot of locations for other devices is an indication of a strange behaviour.
- The onsiderations on the fingerprints for anonimity sets are basically the same
- It would be easy to spot the same 10 devices with the same network connection producing bulks and bulks of new locations, but if they are not phisically in the same place of the population then they would create an unreachable part of the graph, so it's still easy to produce fake data but it's difficult to mix fake data within the real data of the population unless the actors are in the same place 

#### Private data leakage
The key point is not to discolse the real locations unless it's a medical personnel asking, so there is no way to use the hashed location outside of contagion management. I.e. after the contagion, even if the app remains operating, it's leaking no information without someone actively tracking with the same app the same place. Notice that if someone does, he/she would just see the people in and out anyway, so the app would be helping them to collect data but not reduce their practical effort to collect the data.

When hashed locations are sent almost in real time, the negotatied timestamp has a smaller interval so it loses some of its hiding power. Nonetheless, timestamps are easy to generate so the system should not rely on those for security anyway.

Notice that the two devices, when they negotiate the location and timestamp, exchange the recent locations within 100m in clear, on the other hand, the two devices are phisically there so the users are seeing each other.
