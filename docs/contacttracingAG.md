
# Privacy-Preserving Contact Tracing Apple & Google (DP-3T style)

- [Apple Contact Tracing](https://www.apple.com/covid19/contacttracing)
- [Cryptography Specifications](https://covid19-static.cdn-apple.com/applications/covid19/current/static/contact-tracing/pdf/ContactTracing-CryptographySpecification.pdf)

The process description reflectes what I could infer from the specifics, the actual implementation could be quite different:

- every device creates it's own master key, from which daily it derives a daily key
- the day is split into 10-minutes windows, represented by numbers [0..143], for each of these window a proximity key is derived from the daily key _hmac(daily tracing key, time window)_
- when a device is in proximity, such proximity keys are exchanged from one device to the other, so that device A has record of the proximity key of device B and vice versa
- when a patient is tested positive for covid-19, only then the daily keys are uploaded from the patient's device to C, the list of days in which the patient was contagious is also uploaded alongside the daily keys
- other devices can download this set of "infectious" keys and reconstruct, given the days and the [0..143] daily slots, the proximity keys of the infectious users in order to check if they met one of those proximity keys

In this scenario C can be an FTP point or some form of central server which collects the "infectious keys and list of days" of the devices.

The main issues with this model are:
- All the data can be synthetic, in fact there is no need for "witnesses" to generate such proximity keys, on the other hand, only after the medical personnel inspects the patient, the data is shared with the network, so although it's easy to generate such keys, it's impossible for users to post them in the "infectious" list by themselves. This means that the population overview of the contagion is accurate but lagging (you have to wait for official testing, there is no "self reporting")
- After the daily keys are made public, anyone can claim they were in touch with a contagious key, they just prepare the hmac payload and add it to their local storage
- As observed by Marlinspike, the volume of these keys can be huge so even if the keys don't contain location information, it is very likely that location meta-data is needed in order for devices to filter contagious keys and not download the whole set.
- Even if it's "computationally infeasible" to guess the proximity keys, the data part of the hmac function is fixed (basically the [0..143] array), so one can pre-compute at will[* see considerations at the bottom of the page], this gives me an uneasy feeling but it's still a huge search space.

#### Patient History

The app has all the proximity keys encountered in the past, the medical personnel doesn't collect more information about where the patient was from the app, but they can indeed see if the patient was in contact with contagious proximity keys. Basically the proximity keys system builds a social graph and the medical personnel can see how many times the patient was in contact with contagious people. Actual locations have to be asked in the standard way (unless some meta-data about the proximity key is reported within C and made available to the medical personnel)

#### Alerts for users

Users cannot report symptons or infection status on their own: this removes false positives from the network but the users receive feedbacks on their past only after the testing is done on their contacts. There is no close to real-time interactions predicted for now.

#### Epidemiology

C can't know who was in contact with who unless the two people both report in. C only sees lists of infectious keys but has no idea of what the broader population is doing, C only see confirmed infected keys.

The social graph and can be built incrementally while people reports in. 

Unless meta-data is provided to C, it's impossible to build a topological map of the infection.

#### Untruthful user reporting

- So Bob is sick but he won't push the button to signal the netowrk. There is no button to push, Bob will eventually be hositalised and the medical personnel will upload his keys.
- Bob finds out that he was in touch with a contagious key, because he downloads the infected keys and finds a match on his device, there is no way for C to know that Bob was in contact with a contagious key and Bob can remove the tinted key from history and maintain a "clean" history, there is no way for C to detect this behavior from Bob, Bob can still participate in the network.
- Bob is not sick, but he really wants to be for whatever reason. There is no button to push here, but Bob can indeed download the list of contagious keys, build the proximity keys and add them to his history, so he now can prove he met someone contagious. The tricky part for Bob would be to find a key which matches a meaningful location for himself, so, although possible, without location metadata Bob can easily add contagious proximity keys but he cannot know if those keys were actually from his region. So, even if in the specifics location metadata is not collected, in practice it would be important to collect it in order to address this type of behaviour

#### Troll farms
- Although malicious users cannot upload bogus data easily, they can download contagious proximity keys and impersonate them. This requires a person in place to plant the data. If the scope is only creating bogus data, then this is enough. As for the last point of Bob in the previous section, when location metadata is not used, it becomes somehow easy to fake contagion.
- The considerations on the fingerprints are less relevant with this approach because malicious users cannot really upload data to the network. It could be useful to collect the fingerprint of the device along the metadata of a proximity key in order to be able to detect the malicious device when it tries to give expired contagious keys. 

#### Private data leakage
- C a priori sees very little of the users, because C only receives from devices a list of proximity keys and days, nonetheless the location metadata is quite important to offer filtered lists of contagious keys to users and to avoid untruhful reporting (eventually also malicious reporting)
- Users can't really know much about which user got them the contagion, in fact devices download sort of "anonimity sets" where all daily keys looks the same.

---

#### Computationally Infeasible

Guessing 16 bytes numbers point blank is difficult indeed: 2^128 possible combinations.

RPIs originate from another 16 bytes number, the Daily Key, and a known data package combined via hmac, roughly hmac(key,data) = hash(key || hash(key||data)). So to guess the Daily Key you hmac it with all the 144 possible known data packages and then match the results with known RPIs.

For every 16 bytes random number you can create 144 RPIs. Notice that from a bogus data creation point of view, we don't care to find exactly the combination with the right data package, we can be happy to find just a collision. 

- the fastest hashing network known to me is Bitcoin, which today can pump around ~100Ehash/s. Notice that Bitcoin was not build to purely maximize hash/seconds, so there may be faster centralised networks
- for each random number we can compute 144 tentative RPIs, each requires 2 hashes because it's generated via hmac
- let's say that generating the random number is free, so we can create 10^20/288 tentative RPIs per second (~ 2^51), let's also say that checking the result is free too (it's not, but just for simplicity)
- say that you collect almost an entire day of real RPIs from someone, so you'll have 100 real RPIs to match against (~2^6 real samples)
- say you have an entire day to reconstruct that Daily Key, you have 2^16 seconds available
- This gives you 2^16 2^51 2^6 tries against 2^128, giving a very iffy probability of 1/2^55
- so Bitcoin network in one day is not capable of reconstructing a Daily key even with 100 samples.

The assumption part is the "100 samples from 1 key at most 1 day after", which looks reasonable if you want to interact real time.

If you push with the fantasy, let's say you have access to 1M real RPIs (2^20), if you still want to do it in one day, to have a shot you need a 2^84 hash/second monster (120-20-16). That's the number and it's very far (I think?) from reality.



